/**
 * Created by surmilo on 4/12/2016.
 */

public class BasicOperators {
    public static void main(String []args) {
        int x = 25;
        int y = 5;

        //Арифметические операторы//
        System.out.println(x + " + " + y + " = " + (x + y));
        System.out.println(x + " - " + y + " = " + (x - y));
        System.out.println(x + " * " + y + " = " + (x * y));
        System.out.println(x + " / " + y + " = " + (x / y));
        System.out.println();

        //Операторы сравнения//
        System.out.println(x + " < " + y + " = " + (x < y));
        System.out.println(x + " > " + y + " = " + (x > y));
        System.out.println(x + " >= " + y + " = " + (x >= y));
        System.out.println(x + " <= " + y + " = " + (x <= y));
        System.out.println(x + " == " + y + " = " + (x == y));
        System.out.println(x + " != " + y + " = " + (x != y));
        System.out.println();

        //Логические операторы//
        System.out.println("true" + " || " + "true" + " = " + (true || true));
        System.out.println("true" + " || " + "false" + " = " + (true || false));
        System.out.println("false" + " || " + "true" + " = " + (false || true));
        System.out.println("false" + " || " + "false" + " = " + (false || false));
        System.out.println();


        System.out.println("true" + " && " + "true" + " = " + (true && true));
        System.out.println("true" + " && " + "false" + " = " + (true && false));
        System.out.println("false" + " && " + "true" + " = " + (false && true));
        System.out.println("false" + " && " + "false" + " = " + (false && false));
        System.out.println();

        System.out.println("!" + "true" + " = " + (!true));
        System.out.println("!" + "false" + " = " + (!false));
        System.out.println();

        //Операторы присваивания//
        System.out.println("new x" + " = " + (x = y) );



    }}