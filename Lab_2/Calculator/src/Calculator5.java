import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by surmilo on 4/14/2016.
 */
public class Calculator5 {

    public static void main(String[] args) throws IOException {

        System.out.println("Введите первое число");
        BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
        String inputValue1 = br1.readLine();
        double x = Double.parseDouble(inputValue1);

        System.out.println("Введите знак математической операции");
        BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
        String inputValue2 = br2.readLine();

        System.out.println("Введите второе число");
        BufferedReader br3 = new BufferedReader(new InputStreamReader(System.in));
        String inputValue3 = br3.readLine();
        double y = Double.parseDouble(inputValue3);

        double result = 0;

        switch (inputValue2) {
            case "+":
                result = x + y;
                break;

            case "-":
                result = x - y;
                break;
            case "/":
                result = x / y;
                if (y == 0) {
                    System.out.println("Делить на ноль нельзя, попробуйте другое число");
                }
                break;
            case "*":
                result = x * y;
                break;
        }


        long TotalResult = (long) result;
        if (result == TotalResult) {
            System.out.println("Результат равен " + TotalResult);
        } else {
            System.out.println("Результат равен " + result);


        }
    }
}
