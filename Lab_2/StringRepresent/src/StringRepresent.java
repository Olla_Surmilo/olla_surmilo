/**
 * Created by surmilo on 4/12/2016.
 */
public class StringRepresent {
    public static void main(String [ ] args){

        //byte to String
        byte e = 85;
        System.out.println(Byte.toString(e));

        //short to String
        short f = 22015;
        System.out.println(Short.toString(f));

        //int to String
        int a = 1000000;
        System.out.println(Integer.toString(a));

        //double to String
        double  b = 15.4e10;
        System.out.println(Double.toString(b));

        //long to String
        long  c = 294967295;
        System.out.println(Long.toString(c));

        //float to String
        float  d = 5.67f;
        System.out.println(Float.toString(d));

        System.out.println();

        //string to byte
        String str1 = "100";
        System.out.println(Byte.parseByte(str1));

        //string to short
        String str2 = "22015";
        System.out.println(Short.parseShort(str2));

        //string to int
        String str3 = "1000000";
        System.out.println(Integer.parseInt(str3));

        //string to double
        String str4 = "15.4e10";
        System.out.println(Double.parseDouble(str4));

        //string to long
        String str5 = "294967295";
        System.out.println(Long.parseLong(str5));

        //string to float
        String str6 = "36.7";
        System.out.println(Float.parseFloat(str6));

           }
    }

