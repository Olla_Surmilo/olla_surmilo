/**
 * Created by Olya on 13.04.2016.
 */
public class IntFloat {
            public static void main(String []args) {

                // int to float
                int x = 2099;
                System.out.println((float) (x));

                //float to int
                float y = 2.19f;
                System.out.println((int) (y));

}}
