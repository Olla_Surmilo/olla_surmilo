import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static jdk.nashorn.internal.runtime.JSType.isNumber;

/**
 * Created by surmilo on 4/14/2016.
 */


public class SumNumber {

    public static void main(String []args) throws IOException {

        System.out.println("Введите числа, сумма которых должна быть посчитана");

        double sum = 0.0;
        String inputValue1 = "";

        while (true) {

            BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
             inputValue1 = br1.readLine();

            if (inputValue1.equals("сумма")) break;


            sum +=Double.parseDouble(inputValue1);
        }

        long TotalSum = (long) sum;
        if (sum == TotalSum) {
            System.out.println("Сумма: " + TotalSum);
        } else {
            System.out.println("Сумма " + sum);}
    }}
