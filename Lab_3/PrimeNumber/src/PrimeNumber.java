import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by surmilo on 4/14/2016.
 */
public class PrimeNumber {
    public static void main(String[] args) throws IOException {

        System.out.println("Введите число");
        BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
        String inputValue1 = br1.readLine();
        int n = Integer.parseInt(inputValue1);

        System.out.println("Простые числа в диапазоне до " + n + ":");
        System.out.println(2);
        prime:
        for (int i = 3; i <= n; i+=2) {
            for (int j = 2; j <= (int) Math.sqrt(i); j++) {
                if (i % j == 0) continue prime;
            }


            System.out.println(i);
        }
    }
}
