/**
 * Created by surmilo on 4/14/2016.
 */
public class MinArray {
    public static void main(String[] args) {


        int[] number = {9, 8, 255, 3};

        int minItem = number[0];
        for (int i = 1; i < number.length; i++) {

            if (minItem > number[i])
                minItem = number[i];
        }

        System.out.println(" Минимальное число массива: " + minItem);


    }
}

